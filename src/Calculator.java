
public class Calculator {

	private int currentNumber;
	private boolean isAdding = true;
	
	public void insertNumber(int number) {
		if(isAdding) {
			currentNumber += number;
		} else {
			currentNumber = number;
		}
	}
	
	public int GetResult() {
		return currentNumber;
	}
	
	public void Addition() {
		isAdding = true;
	}
}
